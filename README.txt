This Ray Tracer is written in C. It generates a "creative" ray-traced
image (img2.ppm). 

Primitives: spheres, planes

To run the following application,
	1) Download ray_tracer_c.tar.gz from
	https://bitbucket.org/fmontei/ray-tracer-c/downloads
	
	2) From the command line, type make to compile/execute the project
	Note: if the executable creative already exists, execute creative instead
	
	3) img2.ppm is automatically generated
	
	Note: an image viewer compatible with .ppm files is required
