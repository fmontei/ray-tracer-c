#include <math.h>
#include <stdbool.h>
#include "ray.h"
#include "vector.h"

/* Sphere lighting */
COLOR_T get_color (OBJ_T obj, VEC_PT int_pt) 
{
    return obj.color;  
}

/* Plane lighting */
COLOR_T checkerboard (OBJ_T obj, VEC_PT int_pt) 
{
    if (((int) floor(int_pt.x)) + ((int) floor(int_pt.y)) + ((int) floor(int_pt.z)) & 1)
        return obj.color; 

    else
        return obj.color2; 
}

bool shadow_test (OBJ_T objs[], RAY_T ray, VEC_PT int_pt, VEC_PT L, int closest_obj)
{
    int i; 
    VEC_PT norm; 
    double t; 

    // The intersection point on the plane becomes the new ray origin
    ray.or = int_pt; 
    ray.dir = L;     

    for (i = 0; i < OBJ_NUM; ++i) 
    {
        if (i != closest_obj) 
        {
            if (objs[i].intersect(objs[i], ray, &int_pt, &norm, &t) == true)
                return true;
        } 
    }  
}

COLOR_T do_lighting (SCENE_T scene, RAY_T ray, VEC_PT int_pt, VEC_PT norm, int c_obj)
{
    int i; 
    const int n = 100;                 
    const double amb_factor = 0.10; 
    const double c1 = .002, c2 = .02, c3 = .2; 
    double atten, Dl;    
    VEC_PT L, R;             
    COLOR_T object_color, final_color;

    // Function pointer that calls either checkerboard (for planes)
    // or get_color (for spheres)
    object_color = scene.objs[c_obj].color_ptr(scene.objs[c_obj], int_pt);   

    /* Ambient Lighting */
    final_color.r = amb_factor * object_color.r; 
    final_color.g = amb_factor * object_color.g;
    final_color.b = amb_factor * object_color.b;

    /* For diffuse and specular lighting, compute light 
       intensity and direction for each light source in the scene */ 
    for ( i = 0; i < LIGHT_NUM; ++i )
    {

    	/* Calculate Vector L */
    	L = vec_subtract (scene.lights[i].source, int_pt); 

    	/* Light attenuation 
      	  |L| = magnitude of L = Dl */
    	Dl = vec_len (L); 	       
    	atten = 1 / (c1 * Dl * Dl + c2 * Dl + c3); 

    	L = vec_normalize (L);

    	if (shadow_test(scene.objs, ray, int_pt, L, c_obj) == false)
    	{
            if (vec_dot(norm, L) > 0) 
       	    {
	   	/* Diffuse lighting */
           	final_color.r += vec_dot(norm, L) * object_color.r * atten * scene.lights[i].intensity.r; 
           	final_color.g += vec_dot(norm, L) * object_color.g * atten * scene.lights[i].intensity.g; 
           	final_color.b += vec_dot(norm, L) * object_color.b * atten * scene.lights[i].intensity.b; 
    
           	R.x = L.x - 2 * vec_dot(norm, L) * norm.x; 
           	R.y = L.y - 2 * vec_dot(norm, L) * norm.y;
           	R.z = L.z - 2 * vec_dot(norm, L) * norm.z;

           	R = vec_normalize (R);

           	if ( vec_dot(R, ray.dir) > 0 )     
           	{
               	    /* Specular lighting */
               	    final_color.r += pow (vec_dot(R, ray.dir), n) * atten * scene.lights[i].intensity.r; 
               	    final_color.g += pow (vec_dot(R, ray.dir), n) * atten * scene.lights[i].intensity.g;  
               	    final_color.b += pow (vec_dot(R, ray.dir), n) * atten * scene.lights[i].intensity.b;  
               }
           }
        }
    }

    return final_color;  
} 

