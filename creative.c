#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "ray.h"
#include "sphere.h"
#include "vector.h"
#include "light.h"
#include "plane.h"


//////////////////////////////////////////
// Felipe Monteiro                      //
// cpsc102                              //
// Dr. Davis                            // 
// May 22, 2013                         //
// Project 2 -- Intermediate Ray Tracer //
//              Creative Project        //
//                                      //
// Note: Please see ray.c for a full    //
// program description                  //
//////////////////////////////////////////


/* Ray-specific function prototypes */ 
void aspect_ratio (double *, double *);  

bool intersect_plane (OBJ_T, RAY_T, VEC_PT *, VEC_PT *, double *); 

COLOR_T trace (FILE *, SCENE_T, RAY_T);

void print_pixel (FILE *fp, COLOR_T color);


void define_objects (SCENE_T *);

int main (void)
{  
    FILE *fp;
    int x, y;
    double denominator, ratio; 
    RAY_T ray;
    COLOR_T color;  
    SCENE_T scene;

    fp = fopen ("img2.ppm", "w");  
    
    aspect_ratio (&denominator, &ratio);  // Calculate aspect ratio
    define_objects (&scene);                // Define objects

    fprintf (fp, "P6\n%d %d\n%d\n", WIDTH, HEIGHT, MAXV);
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < HEIGHT; ++y ) 
    {
         for ( x = 0; x < WIDTH; ++x ) 
         {  
              ray.or = (VEC_PT) {0, 0, 0};

              // Convert ray.dir from 2D space to 3D space
              if (WIDTH > HEIGHT) 
              {
                  ray.dir.x = -ratio + ((double) x * denominator); 
                  ray.dir.y =  0.5   - ((double) y * denominator); 
                  ray.dir.z =  1.0;
              }
      
              else 
              {
                  ray.dir.x = -0.5   + ((double) x * denominator); 
                  ray.dir.y =  ratio - ((double) y * denominator); 
                  ray.dir.z =  1.0;
              }

              ray.dir = vec_normalize (ray.dir);      // Normalize ray direction
              color = trace (fp, scene, ray); 
              print_pixel (fp, color); 
         } 
    } 

    fprintf (stderr, "Rendering complete. Image saved as img2.ppm\n");
    
    fclose (fp); 

    return 0; 
}

void aspect_ratio (double *denominator, double *ratio)
{
    int numerator; 

    if (WIDTH > HEIGHT) 
    {
        *denominator = HEIGHT; 
        numerator    = WIDTH; 
    }

    /* If the height and with are equal, set denominator equal to whichever */
    else
    {
        *denominator = WIDTH;
        numerator    = HEIGHT; 
    }

   *ratio = ((double) numerator / *denominator) / 2; 
   *denominator = 1.0 / *denominator; 
}

void define_objects (SCENE_T *scene)
{
    /* Background */
    scene->background = (COLOR_T) {0.3, 0.1, 0.5};

    /* Lights */
    scene->lights[0].source    = (VEC_PT) {-10, 1.1, 33};
    scene->lights[0].intensity = (COLOR_T) {1, 1, 1};

    scene->lights[1].source    = (VEC_PT) {-3, 8, 0};
    scene->lights[1].intensity = (COLOR_T) {0.6, 0.6, 0.6};

    scene->lights[2].source    = (VEC_PT) {-7, 13, 30};
    scene->lights[2].intensity = (COLOR_T) {0.4, 0.4, 0.4};

    scene->lights[3].source    = (VEC_PT) {-70, 13, 70};
    scene->lights[3].intensity = (COLOR_T) {0.7, 0.7, 0.7};

    /* Planes */
    scene->objs[0].type = 1; 
    scene->objs[0].intersect = intersect_plane; 
    scene->objs[0].geom.plane.norm = (VEC_PT) {0, 2, 0};
    scene->objs[0].geom.plane.D = 13;
    scene->objs[0].color  = (COLOR_T) {0.3, 0, 0};
    scene->objs[0].color2 = (COLOR_T) {0, 0, 0.5};
    scene->objs[0].color_ptr = checkerboard; 

    scene->objs[1].type = 1;
    scene->objs[1].intersect = intersect_plane;
    scene->objs[1].geom.plane.norm = (VEC_PT) {-2, 0, 0};
    scene->objs[1].geom.plane.D = 15; 
    scene->objs[1].color  = (COLOR_T) {1, 1, 1};
    scene->objs[1].color2 = (COLOR_T) {0.3, 0.3, 0.3};
    scene->objs[1].color_ptr = checkerboard; 
    
    /* Spheres */
    scene->objs[2].type = 0;
    scene->objs[2].intersect = intersect_sphere; 
    scene->objs[2].geom.sphere.radius = 0.55;
    scene->objs[2].geom.sphere.center = (VEC_PT) {-7, -5.8, 35}; 
    scene->objs[2].color = (COLOR_T) {0.8, 0, 0};
    scene->objs[2].color_ptr = get_color; 

    scene->objs[3].type = 0;
    scene->objs[3].intersect = intersect_sphere; 
    scene->objs[3].geom.sphere.radius = 1.15;
    scene->objs[3].geom.sphere.center = (VEC_PT) {-7, -0.1, 35}; 
    scene->objs[3].color = (COLOR_T) {0.5, 0, 0};
    scene->objs[3].color_ptr = get_color; 

    scene->objs[4].type = 0;
    scene->objs[4].intersect = intersect_sphere; 
    scene->objs[4].geom.sphere.radius = 1.5;
    scene->objs[4].geom.sphere.center = (VEC_PT) {-7, 2.2, 34.9}; 
    scene->objs[4].color = (COLOR_T) {0, 0, 0.5};
    scene->objs[4].color_ptr = get_color; 

    scene->objs[5].type = 0;
    scene->objs[5].intersect = intersect_sphere; 
    scene->objs[5].geom.sphere.radius = 1.8;
    scene->objs[5].geom.sphere.center = (VEC_PT) {-7, 5, 34.8}; 
    scene->objs[5].color = (COLOR_T) {0.5, 0, 0};
    scene->objs[5].color_ptr = get_color; 

    scene->objs[6].type = 0;
    scene->objs[6].intersect = intersect_sphere; 
    scene->objs[6].geom.sphere.radius = 0.6;
    scene->objs[6].geom.sphere.center = (VEC_PT) {-0.1, -0.5, 7}; 
    scene->objs[6].color = (COLOR_T) {0.2, 0.3, 0.4};
    scene->objs[6].color_ptr = get_color; 

    scene->objs[7].type = 0;
    scene->objs[7].intersect = intersect_sphere; 
    scene->objs[7].geom.sphere.radius = 0.4;
    scene->objs[7].geom.sphere.center = (VEC_PT) {0.4, -1.1, 6.5}; 
    scene->objs[7].color = (COLOR_T) {0.8, 0.3, 0.3};
    scene->objs[7].color_ptr = get_color; 

    scene->objs[8].type = 0;
    scene->objs[8].intersect = intersect_sphere; 
    scene->objs[8].geom.sphere.radius = 1;
    scene->objs[8].geom.sphere.center = (VEC_PT) {6.1, -0.7, 13}; 
    scene->objs[8].color = (COLOR_T) {0.7, 0.7, 0.7};
    scene->objs[8].color_ptr = get_color; 

    scene->objs[9].type = 0;
    scene->objs[9].intersect = intersect_sphere; 
    scene->objs[9].geom.sphere.radius = 0.99;
    scene->objs[9].geom.sphere.center = (VEC_PT) {6.1, 1.4, 12.5}; 
    scene->objs[9].color = (COLOR_T) {0.95, 0.95, 0.95};
    scene->objs[9].color_ptr = get_color; 

    scene->objs[10].type = 0;
    scene->objs[10].intersect = intersect_sphere; 
    scene->objs[10].geom.sphere.radius = 0.7;
    scene->objs[10].geom.sphere.center = (VEC_PT) {-7, -4.8, 35.0}; 
    scene->objs[10].color = (COLOR_T) {0, 0, 0.5};
    scene->objs[10].color_ptr = get_color;

    scene->objs[11].type = 0;
    scene->objs[11].intersect = intersect_sphere; 
    scene->objs[11].geom.sphere.radius = 0.8;
    scene->objs[11].geom.sphere.center = (VEC_PT) {-7, -3.5, 35.0}; 
    scene->objs[11].color = (COLOR_T) {0.5, 0, 0};
    scene->objs[11].color_ptr = get_color;

    scene->objs[12].type = 0;
    scene->objs[12].intersect = intersect_sphere; 
    scene->objs[12].geom.sphere.radius = 0.95;
    scene->objs[12].geom.sphere.center = (VEC_PT) {-7, -1.9, 35.0}; 
    scene->objs[12].color = (COLOR_T) {0, 0, 0.5};
    scene->objs[12].color_ptr = get_color;

    scene->objs[13].type = 0;
    scene->objs[13].intersect = intersect_sphere; 
    scene->objs[13].geom.sphere.radius = 2.3;
    scene->objs[13].geom.sphere.center = (VEC_PT) {-7, 8.4, 34.7}; 
    scene->objs[13].color = (COLOR_T) {0, 0, 0.5};
    scene->objs[13].color_ptr = get_color;

    scene->objs[14].type = 0;
    scene->objs[14].intersect = intersect_sphere; 
    scene->objs[14].geom.sphere.radius = 3;
    scene->objs[14].geom.sphere.center = (VEC_PT) {-7, 13.5, 34.6}; 
    scene->objs[14].color = (COLOR_T) {0.5, 0, 0};
    scene->objs[14].color_ptr = get_color;

    scene->objs[15].type = 0;
    scene->objs[15].intersect = intersect_sphere; 
    scene->objs[15].geom.sphere.radius = 5;
    scene->objs[15].geom.sphere.center = (VEC_PT) {-7, 20.4, 34}; 
    scene->objs[15].color = (COLOR_T) {0, 0, 0.5};
    scene->objs[15].color_ptr = get_color;

    scene->objs[16].type = 0;
    scene->objs[16].intersect = intersect_sphere; 
    scene->objs[16].geom.sphere.radius = 0.8;
    scene->objs[16].geom.sphere.center = (VEC_PT) {4.5, -2, 12}; 
    scene->objs[16].color = (COLOR_T) {0.6, 0.6, 0.6};
    scene->objs[16].color_ptr = get_color; 
     
    scene->objs[17].type = 0;
    scene->objs[17].intersect = intersect_sphere; 
    scene->objs[17].geom.sphere.radius = 0.65;
    scene->objs[17].geom.sphere.center = (VEC_PT) {2.6, -2.3, 11}; 
    scene->objs[17].color = (COLOR_T) {0.5, 0.5, 0.5};
    scene->objs[17].color_ptr = get_color; 

    scene->objs[18].type = 0;
    scene->objs[18].intersect = intersect_sphere; 
    scene->objs[18].geom.sphere.radius = 0.6;
    scene->objs[18].geom.sphere.center = (VEC_PT) {1.95, -1.5, 11.4}; 
    scene->objs[18].color = (COLOR_T) {0.4, 0.4, 0.4};
    scene->objs[18].color_ptr = get_color; 

    scene->objs[19].type = 0;
    scene->objs[19].intersect = intersect_sphere; 
    scene->objs[19].geom.sphere.radius = 0.5;
    scene->objs[19].geom.sphere.center = (VEC_PT) {2.5, -0.4, 11.7}; 
    scene->objs[19].color = (COLOR_T) {0.3, 0.3, 0.3};
    scene->objs[19].color_ptr = get_color;

    scene->objs[20].type = 0;
    scene->objs[20].intersect = intersect_sphere; 
    scene->objs[20].geom.sphere.radius = 0.45;
    scene->objs[20].geom.sphere.center = (VEC_PT) {3.1, 0.44, 11.7}; 
    scene->objs[20].color = (COLOR_T) {0.2, 0.2, 0.2};
    scene->objs[20].color_ptr = get_color;

    scene->objs[21].type = 0;
    scene->objs[21].intersect = intersect_sphere; 
    scene->objs[21].geom.sphere.radius = 0.4;
    scene->objs[21].geom.sphere.center = (VEC_PT) {3.7, 1.3, 11.6}; 
    scene->objs[21].color = (COLOR_T) {0.1, 0.1, 0.1};
    scene->objs[21].color_ptr = get_color;

    scene->objs[22].type = 0;
    scene->objs[22].intersect = intersect_sphere; 
    scene->objs[22].geom.sphere.radius = 0.38;
    scene->objs[22].geom.sphere.center = (VEC_PT) {5, 2.3, 13.5}; 
    scene->objs[22].color = (COLOR_T) {0.1, 0.1, 0.2};
    scene->objs[22].color_ptr = get_color;

    scene->objs[23].type = 0;
    scene->objs[23].intersect = intersect_sphere; 
    scene->objs[23].geom.sphere.radius = 0.33;
    scene->objs[23].geom.sphere.center = (VEC_PT) {5.8, 3, 13.5};
    scene->objs[23].color = (COLOR_T) {0, 0, 0.2};
    scene->objs[23].color_ptr = get_color;

    scene->objs[24].type = 0;
    scene->objs[24].intersect = intersect_sphere; 
    scene->objs[24].geom.sphere.radius = 0.3;
    scene->objs[24].geom.sphere.center = (VEC_PT) {6.7, 3.2, 13.4};
    scene->objs[24].color = (COLOR_T) {0, 0, 0.3};
    scene->objs[24].color_ptr = get_color;
    
    scene->objs[25].type = 0;
    scene->objs[25].intersect = intersect_sphere; 
    scene->objs[25].geom.sphere.radius = 0.27;
    scene->objs[25].geom.sphere.center = (VEC_PT) {7.35, 3.05, 13};
    scene->objs[25].color = (COLOR_T) {0, 0, 0.4};
    scene->objs[25].color_ptr = get_color;

    scene->objs[26].type = 0;
    scene->objs[26].intersect = intersect_sphere; 
    scene->objs[26].geom.sphere.radius = 0.2;
    scene->objs[26].geom.sphere.center = (VEC_PT) {6.6, 2, 11};
    scene->objs[26].color = (COLOR_T) {0, 0, 0.5};
    scene->objs[26].color_ptr = get_color;

    scene->objs[27].type = 0;
    scene->objs[27].intersect = intersect_sphere; 
    scene->objs[27].geom.sphere.radius = 0.15;
    scene->objs[27].geom.sphere.center = (VEC_PT) {6.8, 1.4, 11};
    scene->objs[27].color = (COLOR_T) {0, 0, 0.6};
    scene->objs[27].color_ptr = get_color;

    scene->objs[28].type = 0;
    scene->objs[28].intersect = intersect_sphere; 
    scene->objs[28].geom.sphere.radius = 0.1;
    scene->objs[28].geom.sphere.center = (VEC_PT) {6.8, 0.75, 11};
    scene->objs[28].color = (COLOR_T) {0, 0, 0.8};
    scene->objs[28].color_ptr = get_color;

    scene->objs[29].type = 0;
    scene->objs[29].intersect = intersect_sphere; 
    scene->objs[29].geom.sphere.radius = 0.09;
    scene->objs[29].geom.sphere.center = (VEC_PT) {6.8, 0.2, 11};
    scene->objs[29].color = (COLOR_T) {0.1, 0, 1};
    scene->objs[29].color_ptr = get_color;

    scene->objs[30].type = 0;
    scene->objs[30].intersect = intersect_sphere; 
    scene->objs[30].geom.sphere.radius = 0.11;
    scene->objs[30].geom.sphere.center = (VEC_PT) {6.75, -0.28, 11};
    scene->objs[30].color = (COLOR_T) {0.2, 0, 0.9};
    scene->objs[30].color_ptr = get_color;

    scene->objs[31].type = 0;
    scene->objs[31].intersect = intersect_sphere; 
    scene->objs[31].geom.sphere.radius = 0.15;
    scene->objs[31].geom.sphere.center = (VEC_PT) {6.6, -0.9, 11};
    scene->objs[31].color = (COLOR_T) {0.3, 0, 0.8};
    scene->objs[31].color_ptr = get_color;

    scene->objs[32].type = 0;
    scene->objs[32].intersect = intersect_sphere; 
    scene->objs[32].geom.sphere.radius = 0.25;
    scene->objs[32].geom.sphere.center = (VEC_PT) {6.4, -1.6, 11};
    scene->objs[32].color = (COLOR_T) {0.5, 0, 0.8};
    scene->objs[32].color_ptr = get_color;

    scene->objs[33].type = 0;
    scene->objs[33].intersect = intersect_sphere; 
    scene->objs[33].geom.sphere.radius = 0.3;
    scene->objs[33].geom.sphere.center = (VEC_PT) {6, -2.4, 11};
    scene->objs[33].color = (COLOR_T) {0.6, 0, 0.6};
    scene->objs[33].color_ptr = get_color;

    scene->objs[34].type = 0;
    scene->objs[34].intersect = intersect_sphere; 
    scene->objs[34].geom.sphere.radius = 0.35;
    scene->objs[34].geom.sphere.center = (VEC_PT) {5.5, -3.3, 11};
    scene->objs[34].color = (COLOR_T) {0.8, 0, 0.6};
    scene->objs[34].color_ptr = get_color;

    scene->objs[35].type = 0;
    scene->objs[35].intersect = intersect_sphere; 
    scene->objs[35].geom.sphere.radius = 0.4;
    scene->objs[35].geom.sphere.center = (VEC_PT) {4.8, -4.2, 12};
    scene->objs[35].color = (COLOR_T) {0.8, 0, 0.3};
    scene->objs[35].color_ptr = get_color;

    scene->objs[36].type = 0;
    scene->objs[36].intersect = intersect_sphere; 
    scene->objs[36].geom.sphere.radius = 0.55;
    scene->objs[36].geom.sphere.center = (VEC_PT) {4, -5, 15};
    scene->objs[36].color = (COLOR_T) {1, 0, 0};
    scene->objs[36].color_ptr = get_color;
}

COLOR_T trace (FILE *fp, SCENE_T scene, RAY_T ray) 
{
    int i; 
    int c_obj = -1;  // c_obj is the closest object
    double t;
    double closest_t = 1000.0; 
    VEC_PT intersect_pt, norm; 
    VEC_PT closest_int_pt, closest_norm; 
   
    for ( i = 0; i < OBJ_NUM; ++i ) 
    {
       if (scene.objs[i].intersect(scene.objs[i], ray, &intersect_pt, &norm, &t) == true) {

          if (t < closest_t) {
            
             closest_t = t; 
             closest_norm = norm; 
             closest_int_pt = intersect_pt; 
             c_obj = i; 
          }
       }
    }      

    if (c_obj >= 0) {
          
       scene.objs[c_obj].color = do_lighting (scene, ray, closest_int_pt, closest_norm, c_obj);
       return scene.objs[c_obj].color;
    }
    
    else 
       return scene.background;

}

void print_pixel (FILE *fp, COLOR_T color)
{
    if (color.r > 1)
        color.r = 1; 

    if (color.g > 1)
        color.g = 1; 

    if (color.b > 1)
        color.b = 1; 
 
    fprintf (fp, "%c%c%c", (unsigned char) (color.r * 255), 
                           (unsigned char) (color.g * 255),
                           (unsigned char) (color.b * 255));
}
