OBJS = creative.c \
 vector.c \
 sphere.c \
  light.c \
  plane.c

exec: \
compile
	./creative

compile: \
creative.c
	gcc ${OBJS} -o creative -lm
	@echo
	@echo "Compiling Project 2 Creative Image"
	touch compile

clean:
	rm -rf a.out *.o *.err


